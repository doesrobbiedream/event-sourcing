import { Projector } from './projector.class'
import { Projection } from './projection.class'
import { ProjectionArtisan } from './projection.artisan'
import { Event, EventArtisan, SourceEvent } from '../event-artisan'
import { delay, firstValueFrom, of, Subject } from 'rxjs'

let hooksExecutionOrder: Array<string> = []
let eventsExecutionOrder: Array<string> = []
let delayedEventCalls = 0

describe(Projector.name, function () {
  let projection: TestProjection
  let projector: BeforeEachOfTypeTest
  beforeEach(() => {
    hooksExecutionOrder = []
    projection = new TestProjection()
    projector = new BeforeEachOfTypeTest(projection)
  })
  describe('when using projection hooks', function () {
    it('should execute them in order', async () => {
      await projector.projectEvents([
        {
          type: 'TestEventOne',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
      ]).batchFinished

      expect(hooksExecutionOrder).toEqual([
        'BeforeAny',
        'BeforeEachOfType',
        'Acquiring',
        'ApplyingEvent',
        'AfterEachOfType',
        'AfterAny',
      ])
    })
  })
  describe('when providing no supported events', function () {
    it('should complete events processing without errors', async () => {
      await expect(
        projector.projectEvents([
          {
            type: 'TestEventThree',
            version: 1,
            data: {},
            meta: {},
            uuid: '',
            createdAt: new Date(Date.now()),
          },
        ]).batchFinished
      ).resolves
    })
  })
  describe('when sending multiple events by stream that delays to complete', function () {
    beforeEach(() => {
      delayedEventCalls = 0
    })
    it('should process all events in order', async () => {
      const events: Array<Event> = Array.from({ length: 3 }).fill({
        type: 'DelayedEvent',
        version: 1,
        data: {},
        meta: {},
        uuid: '',
        createdAt: new Date(Date.now()),
      }) as Array<Event>
      const eventsStream$ = new Subject<Event>()
      projector.attachEventStream(eventsStream$.asObservable())
      events.forEach((event) => eventsStream$.next(event))
      await firstValueFrom(projector.clearedQueue$)
      expect(delayedEventCalls).toEqual(events.length)
    })
  })
  describe('when projecting multiple events', function () {
    beforeEach(() => {
      eventsExecutionOrder = []
    })
    it('should process them in order', async () => {
      const events: Array<Event> = [
        {
          type: 'DelayedEvent',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
        {
          type: 'TestEventOne',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
        {
          type: 'DelayedEvent',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
        {
          type: 'DelayedEvent',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
        {
          type: 'TestEventOne',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
        {
          type: 'TestEventOne',
          version: 1,
          data: {},
          meta: {},
          uuid: '',
          createdAt: new Date(Date.now()),
        },
      ]
      await projector.projectEvents(events).batchFinished
      expect(eventsExecutionOrder).toEqual(events.map((e) => e.type))
    })
  })
})

@EventArtisan.Event({
  type: 'TestEventOne',
  version: 1,
})
export class TestEventOne extends SourceEvent {
  discr = 'STRING'
}

@EventArtisan.Event({
  type: 'DelayedEvent',
  version: 1,
})
export class DelayedEvent extends SourceEvent {
  discr = 'STRING'
}

@EventArtisan.Event({
  type: 'NotSupportedEvent',
  version: 1,
})
export class NotSupportedEvent extends SourceEvent {
  discr = 'STRING'
}

export class TestProjection extends Projection<{ uuid: string }> {
  @ProjectionArtisan.EventApplier(TestEventOne)
  async applyTestEventOne(event: TestEventOne): Promise<void> {
    hooksExecutionOrder.push('ApplyingEvent')
    eventsExecutionOrder.push('TestEventOne')
  }

  @ProjectionArtisan.EventApplier(DelayedEvent)
  async applyTestEventTwo(event: DelayedEvent): Promise<void> {
    delayedEventCalls++
    eventsExecutionOrder.push('DelayedEvent')
    return firstValueFrom(of(null).pipe(delay(1000)))
  }
}

@ProjectionArtisan.Projector(TestProjection)
class BeforeEachOfTypeTest extends Projector<TestProjection> {
  constructor(protected projection: TestProjection) {
    super()
  }

  @ProjectionArtisan.BeforeEachOfType(TestEventOne)
  async beforeTestEventOne() {
    hooksExecutionOrder.push('BeforeEachOfType')
  }

  @ProjectionArtisan.BeforeAny()
  async beforeAny(events: TestEventOne | DelayedEvent) {
    hooksExecutionOrder.push('BeforeAny')
  }

  @ProjectionArtisan.AfterEachOfType(TestEventOne)
  async afterTestEventOne() {
    hooksExecutionOrder.push('AfterEachOfType')
  }

  @ProjectionArtisan.AfterAny()
  async afterAny(projection: Projection<TestProjection>[]) {
    hooksExecutionOrder.push('AfterAny')
  }

  @ProjectionArtisan.ProjectionAcquirer(TestEventOne, DelayedEvent)
  async acquire(): Promise<TestProjection> {
    hooksExecutionOrder.push('Acquiring')
    return this.projection
  }
}
