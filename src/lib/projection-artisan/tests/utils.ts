import { Event } from '../../event-artisan/event.types'
import { randomUUID } from 'crypto'

export type TypeFromStoredEvent<T extends Event> = T['type']
export type DataFromStoredEvent<T extends Event> = T extends Event<infer Data>
  ? Data
  : never

export function createEventFromData<T extends Event>(
  type: TypeFromStoredEvent<T>,
  version: number,
  data: DataFromStoredEvent<T>,
  overwrite: Record<string, unknown> = {}
): T {
  return {
    uuid: randomUUID(),
    type,
    data,
    createdAt: new Date(Date.now()),
    meta: {},
    version,
    ...overwrite,
  } as T
}
