import { ProjectionArtisan } from '../projection.artisan'
import { filter, Subject } from 'rxjs'
import { Event, EventArtisan, SourceEvent, Upcaster } from '../../event-artisan'
import { Projector } from '../projector.class'
import { Projection } from '../projection.class'
import { MockDB } from './mocked-db'
import { randomUUID } from 'crypto'
import { createEventFromData, DataFromStoredEvent } from './utils'

describe(ProjectionArtisan.name, function () {
  describe('Update a projected collection from events', () => {
    /*
     *
     * Feature: Update a projected collection from events [updated-a-projected-collection.feature]
     *   Scenario:  Update the name of an author
     *
     *   Solution Overview
     *   The Projector will be dependent on a ResultsProvider and a Concrete Projection, which is basically a query executor connected to an external DB
     *   It will also link streams of an EventType to a Query that will execute to fetch implicated results
     *   whenever a stream fires with an event.
     *   When the results are fetched, the Projector will generate instances of its Projection and apply the given event to it.
     *
     * */
    describe('Update the name of an author', () => {
      describe('given a persisted ProjectedCollection of Book<{ title: string, author: string }>', function () {
        const BookProjectedCollection: Book[] = [
          { uuid: randomUUID(), title: '', author: 'Tolkien, J. R R.' },
          { uuid: randomUUID(), title: '', author: 'Tolkien, J. R R.' },
          { uuid: randomUUID(), title: '', author: 'Tolkien, J. R R.' },
          { uuid: randomUUID(), title: '', author: 'Asimov, Isaac' },
        ]
        const booksProjectionsDB: MockDB<Book> = new MockDB()

        let eventsStream: Subject<Event>
        let projector: Projector<BookProjection>

        beforeEach(async () => {
          projector = new BooksProjector()
          await booksProjectionsDB.saveAll(BookProjectedCollection)
        })

        describe('when the event AuthorsNameUpdated is fired', function () {
          let authorNameUpdatedEvent: AuthorNameUpdatedStored
          beforeEach(() => {
            authorNameUpdatedEvent =
              createEventFromData<AuthorNameUpdatedStored>(
                'AuthorNameUpdated',
                1,
                {
                  previousName: 'Tolkien, J. R R.',
                  newName: 'Tolkien, J. R. R.',
                }
              )
          })
          it("should update all the books that previously had that author's name", function (done) {
            eventsStream = new Subject()
            projector
              .attachEventStream(eventsStream)
              .afterAppliedStreamedEvent.pipe(
                filter((event) => event.type === 'AuthorNameUpdated')
              )
              .subscribe(async () => {
                const books = await booksProjectionsDB.get()
                expect(
                  books.filter((b) => b.author === 'Tolkien, J. R. R.')
                ).toHaveLength(3)
                done()
              })

            eventsStream.next(authorNameUpdatedEvent)
          })
        })

        interface Book {
          uuid: string
          title: string
          author: string
        }

        interface AuthorNameUpdatedStored extends Event {
          type: 'AuthorNameUpdated'
          data: { previousName: string; newName: string }
        }

        @EventArtisan.Event({ type: 'AuthorNameUpdated', version: 1 })
        class AuthorNameUpdated extends SourceEvent<{
          previousName: string
          newName: string
        }> {}

        class BookProjection extends Projection<Book> {
          uuid: string
          title: string
          author: string

          @ProjectionArtisan.EventApplier(AuthorNameUpdated)
          async setAuthor(event: AuthorNameUpdated) {
            this.author = event.data.newName
          }
        }

        @ProjectionArtisan.Projector(BookProjection)
        class BooksProjector extends Projector<BookProjection> {
          @ProjectionArtisan.AfterEachOfType(AuthorNameUpdated)
          async save(projection: Array<BookProjection>) {
            await booksProjectionsDB.saveAll(
              projection.map((p) => p.serialize())
            )
          }

          @ProjectionArtisan.ProjectionAcquirer(AuthorNameUpdated)
          async fetchBooksByAuthor(
            event: AuthorNameUpdatedStored
          ): Promise<BookProjection[]> {
            return (await booksProjectionsDB.get())
              .filter((b) => b.author === event.data.previousName)
              .map((book) => Projection.fromInitialState(BookProjection, book))
          }
        }
      })
    })

    /*
     *
     * Feature: Update a projected collection from events [updated-a-projected-collection.feature]
     *   Scenario: Update a projection with a specific sequence of events
     *
     * Proposed Test:
     *
     * As is not the same adding two numbers and then divide them than divide the first and then add the second,
     * the proposed test is to have the operations stream as:
     *
     * 1 - Add 9
     * 2 - Divide by 3
     * 3 - Add 3
     *
     * The expected outcome shall be 6 instead of 4
     *
     * In order to ensure order, when dividing,
     * the fetcher will have a delay that would prevent false positive preventing race conditions
     *
     * */
    describe('Update a projection with a specific sequence of events', () => {
      interface SimpleMathOperation extends Event {
        type: 'AddedWith' | 'DividedBy'
        data: {
          uuid: string
          value: number
        }
      }

      @EventArtisan.Event({
        type: 'AddedWith',
        version: 1,
      })
      class AddedWithEvent extends SourceEvent<
        DataFromStoredEvent<SimpleMathOperation>
      > {}

      @EventArtisan.Event({
        type: 'DividedBy',
        version: 1,
      })
      class DividedByEvent extends SourceEvent<
        DataFromStoredEvent<SimpleMathOperation>
      > {}

      describe('given a projection of numbers', function () {
        let projector: ResultProjector
        beforeEach(() => {
          projector = new ResultProjector()
          resultsProjectionStore.saveAll([{ uuid: '1', value: 0 }])
        })

        describe('When multiple operations event are fired', function () {
          let operationEvents: Array<SimpleMathOperation>
          beforeEach(() => {
            operationEvents = [
              createEventFromData<SimpleMathOperation>('AddedWith', 1, {
                uuid: '1',
                value: 9,
              }),
              createEventFromData<SimpleMathOperation>('DividedBy', 1, {
                uuid: '1',
                value: 3,
              }),
              createEventFromData<SimpleMathOperation>('AddedWith', 1, {
                uuid: '1',
                value: 3,
              }),
            ]
          })
          it('should apply all operations in the correct order', async () => {
            await projector.projectEvents(operationEvents).batchFinished
            const result = (await resultsProjectionStore.get())[0]
            expect(result.value).toEqual(6)
          })
        })

        interface IResultProjection {
          uuid: string
          value: number
        }

        const resultsProjectionStore = new MockDB<{
          uuid: string
          value: number
        }>()

        class ResultProjection extends Projection<IResultProjection> {
          uuid: string
          value: number

          @ProjectionArtisan.EventApplier(AddedWithEvent)
          async add(event: AddedWithEvent) {
            this.value += event.data.value
          }

          @ProjectionArtisan.EventApplier(DividedByEvent)
          async divide(event: DividedByEvent) {
            this.value /= event.data.value
          }
        }

        @ProjectionArtisan.Projector(ResultProjection)
        class ResultProjector extends Projector<ResultProjection> {
          @ProjectionArtisan.AfterAny()
          async save(projection: ResultProjection[]) {
            await resultsProjectionStore.saveAll(
              projection.map((p) => p.serialize())
            )
          }

          @ProjectionArtisan.ProjectionAcquirer(AddedWithEvent, DividedByEvent)
          async fetchResult(
            event: SimpleMathOperation
          ): Promise<ResultProjection[]> {
            const result = (await resultsProjectionStore.get())[0]
            const projection = Projection.fromInitialState(
              ResultProjection,
              result
            )
            if (event.type === 'DividedBy') {
              return new Promise<ResultProjection[]>((resolve) =>
                setTimeout(() => resolve([projection]), 500)
              )
            } else {
              return new Promise<ResultProjection[]>((resolve) =>
                setTimeout(() => resolve([projection]), 0)
              )
            }
          }
        }
      })
    })
  })

  describe('Building a projection of words from stored events', () => {
    const eventStore: MockDB<Event> = new MockDB()
    const projectionDB: MockDB<Sentence> = new MockDB()

    type SentenceId = string

    interface Sentence {
      uuid: SentenceId
      sentence: string
    }

    interface WordData {
      sentenceId: SentenceId
      word: string
    }

    interface PositionedWordData extends WordData {
      position: number
    }

    @EventArtisan.Event({
      type: 'InitializeSentence',
      version: 1,
    })
    class InitializeSentence extends SourceEvent<{ uuid: SentenceId }> {}

    @EventArtisan.Event({
      type: 'AddWord',
      version: 2,
    })
    class AddPositionedWord
      extends SourceEvent<PositionedWordData>
      implements Upcaster<AddWord>
    {
      upcast(event: AddWord) {
        this.fromRawData({ ...event.data, position: -1 }, null)
      }
    }

    @EventArtisan.LegacyOf(AddPositionedWord)
    @EventArtisan.Event({
      type: 'AddWord',
      version: 1,
    })
    class AddWord extends SourceEvent<WordData> {}

    @EventArtisan.Event({
      type: 'RemoveWord',
      version: 1,
    })
    class RemoveWord extends SourceEvent<PositionedWordData> {}

    class SentencesProjection extends Projection<Sentence> implements Sentence {
      uuid: SentenceId
      sentence: string

      @ProjectionArtisan.EventApplier(InitializeSentence)
      async initialize(event: InitializeSentence) {
        this.uuid = event.data.uuid
        this.sentence = ''
      }

      @ProjectionArtisan.EventApplier(AddWord, AddPositionedWord)
      async addWord(event: AddPositionedWord) {
        const words = this.sentence.split(' ').filter((w) => w !== '')
        const position =
          event.data.position === -1 ? words.length : event.data.position
        words.splice(position, 0, event.data.word)

        this.sentence = words.join(' ')
      }

      @ProjectionArtisan.EventApplier(RemoveWord)
      async removeWord(event: RemoveWord) {
        const words = this.sentence.split(' ')
        const index = words.indexOf(event.data.word)
        words.splice(index, 1)

        this.sentence = words.join(' ')
      }
    }

    @ProjectionArtisan.Projector(SentencesProjection)
    class SentencesProjector extends Projector<SentencesProjection> {
      constructor(protected projectionStore: MockDB<Sentence>) {
        super()
      }

      @ProjectionArtisan.AfterAny()
      async save(sentences: SentencesProjection[]): Promise<void> {
        await this.projectionStore.saveAll(sentences.map((s) => s.serialize()))
      }

      @ProjectionArtisan.ProjectionAcquirer(InitializeSentence)
      async getNewSentence(): Promise<SentencesProjection> {
        return new SentencesProjection()
      }

      @ProjectionArtisan.ProjectionAcquirer(
        AddWord,
        AddPositionedWord,
        RemoveWord
      )
      async getSentenceByID(
        event: Event<PositionedWordData>
      ): Promise<SentencesProjection> {
        const sentence = await this.projectionStore.getOne(
          event.data.sentenceId
        )
        return Projection.fromInitialState(SentencesProjection, sentence)
      }
    }

    describe('I have an event store of word events', () => {
      describe('and events have different types and versions', () => {
        const sentenceId = randomUUID()
        beforeAll(async () => {
          await eventStore.saveAll([
            createEventFromData<InitializeSentence>('InitializeSentence', 1, {
              uuid: sentenceId,
            }),
            createEventFromData<AddWord>('AddWord', 1, {
              word: 'Hello',
              sentenceId,
            }),
            createEventFromData<AddWord>('AddWord', 1, {
              word: 'from',
              sentenceId,
            }),
            createEventFromData<AddWord>('AddWord', 1, {
              word: 'Argentina',
              sentenceId,
            }),
            createEventFromData<AddWord>('RemoveWord', 1, {
              word: 'Argentina',
              sentenceId,
            }),
            createEventFromData<AddWord>('AddWord', 1, {
              word: 'Spain!',
              sentenceId,
            }),
            createEventFromData<AddPositionedWord>('AddWord', 2, {
              word: 'World',
              sentenceId,
              position: 1,
            }),
          ])
        })

        describe('when building from scratch the sentences projection', () => {
          let events: Event[]
          beforeEach(async () => {
            events = await eventStore.get()
          })
          it('should get all events processed and my sentences are built', async () => {
            const projector = new SentencesProjector(projectionDB)
            await projector.projectEvents(events).batchFinished
            const projected = await projectionDB.get()
            expect(projected).toEqual([
              { uuid: sentenceId, sentence: 'Hello World from Spain!' },
            ])
          })
        })
      })
    })
  })
})
