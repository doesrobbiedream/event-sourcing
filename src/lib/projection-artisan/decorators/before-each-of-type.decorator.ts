import { Type } from '@doesrobbiedream/ts-utils'
import { SourceEvent } from '../../event-artisan'
import { Projector } from '../projector.class'
import { Projection } from '../projection.class'
import {
  EventType,
  MethodKey,
  ProjectorEventHandlerManifest,
  UnpackArray,
  UnpackType,
} from './helpers/decorators.types'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export function BeforeEachOfTypeDecorator<T extends Type<SourceEvent>[]>(
  ...events: T
) {
  return (
    target: Projector<Projection<unknown>>,
    methodName: MethodKey,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    descriptor: TypedPropertyDescriptor<
      (event: UnpackType<UnpackArray<T>>) => Promise<void>
    >
  ) => {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionBeforeEachOfTypeEventCallback,
        target
      ) || new Map()
    events.forEach((event) => {
      handlers.set(SourceEvent.getSignature(event), {
        handlerKey: methodName,
        Event: event,
      })
    })
    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionBeforeEachOfTypeEventCallback,
      handlers,
      target
    )
  }
}
