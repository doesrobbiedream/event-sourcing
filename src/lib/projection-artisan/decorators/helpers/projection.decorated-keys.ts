export enum ProjectionDecoratedKeys {
  ProjectionType = 'ProjectionType',
  ProjectionAcquirer = 'ProjectorFetchers',
  ProjectionEventHandlers = 'ProjectionEventHandlers',
  ProjectionBeforeAnyEventCallback = 'ProjectionBeforeAnyEventCallback',
  ProjectionBeforeEachOfTypeEventCallback = 'ProjectionBeforeEachOfTypeEventCallback',
  ProjectionAfterEachOfTypeEventCallback = 'ProjectionAfterEachOfTypeEventCallback',
  ProjectionAfterAnyEventCallback = 'ProjectionAfterAnyEventCallback',
}
