// Helpers
import { Event, SourceEvent } from '../../../event-artisan'
import {
  EventProjectorMethod,
  EventType,
  ProjectorEventHandlerManifest,
} from './decorators.types'
import { ProjectionDecoratedKeys } from './projection.decorated-keys'

export const eventApplierHandlerBuilder = (
  original: EventProjectorMethod<SourceEvent>
) =>
  async function eventApplierHandler(event: Event) {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionEventHandlers,
        this
      ) || new Map()
    const identifier = SourceEvent.getSignatureFromRaw(event)
    const EventConstructor = handlers.get(identifier).Event
    const eventInstance = new EventConstructor()
    eventInstance.fromStoredData(event)
    const liftedEventInstance: SourceEvent = eventInstance.lift()
    await original.apply(this, [liftedEventInstance.serialize()])
  }
