import { SourceEvent } from '../../../event-artisan'
import { Type } from '@doesrobbiedream/ts-utils'

export type EventType = string
export type MethodKey = string

export interface ProjectorHandlerManifest {
  handlerKey: string
}

export enum ProjectorHooks {
  beforeAny = 'beforeAny',
  beforeEach = 'beforeEach',
  afterEach = 'afterEach',
  afterAny = 'afterAny',
}

export type HooksMap = Map<string, ProjectorEventHandlerManifest>

export interface ProjectorEventHandlerManifest
  extends ProjectorHandlerManifest {
  Event: Type<SourceEvent>
}

export type EventProjectorMethod<Event extends SourceEvent> = (
  event: Event
) => Promise<void>

export type UnpackArray<T> = T extends Array<infer U> ? U : never
export type UnpackType<T> = T extends Type<infer U> ? U : never
