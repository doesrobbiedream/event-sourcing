import { Projection } from '../projection.class'
import { Type } from '@doesrobbiedream/ts-utils'
import { Projector } from '../projector.class'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export function ProjectorDecorator<P extends Projection<unknown>>(
  projection: Type<P>
) {
  return (target: Type<Projector<P>>) => {
    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionType,
      projection,
      target
    )
  }
}
