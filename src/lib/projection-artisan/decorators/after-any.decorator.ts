import { Projection } from '../projection.class'
import { Projector } from '../projector.class'
import { MethodKey, ProjectorHandlerManifest } from './helpers/decorators.types'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export type AfterAnyEventIsProjectedMethod<P extends Projection<unknown>> = (
  projections: Array<P>
) => Promise<void>

export function AfterAny<P extends Projection<unknown>>() {
  return (
    target: Projector<P>,
    methodName: MethodKey,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    descriptor: TypedPropertyDescriptor<AfterAnyEventIsProjectedMethod<P>>
  ) => {
    const afterEachEventMap: Map<string, ProjectorHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionAfterAnyEventCallback,
        target
      ) || new Map()

    afterEachEventMap.set(methodName, { handlerKey: methodName })

    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionAfterAnyEventCallback,
      afterEachEventMap,
      target
    )
  }
}
