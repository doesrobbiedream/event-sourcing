import { Projection } from '../projection.class'
import { Type } from '@doesrobbiedream/ts-utils'
import { SourceEvent } from '../../event-artisan'
import { Projector } from '../projector.class'

import {
  EventType,
  MethodKey,
  ProjectorEventHandlerManifest,
} from './helpers/decorators.types'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'
import { AfterAnyEventIsProjectedMethod } from './after-any.decorator'

export function AfterEachOfTypeDecorator<P extends Projection<unknown>>(
  ...events: Array<Type<SourceEvent>>
) {
  return (
    target: Projector<P>,
    methodName: MethodKey,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    descriptor: TypedPropertyDescriptor<AfterAnyEventIsProjectedMethod<P>>
  ) => {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionAfterEachOfTypeEventCallback,
        target
      ) || new Map()
    events.forEach((event) => {
      handlers.set(SourceEvent.getSignature(event), {
        handlerKey: methodName,
        Event: event,
      })
    })
    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionAfterEachOfTypeEventCallback,
      handlers,
      target
    )
  }
}
