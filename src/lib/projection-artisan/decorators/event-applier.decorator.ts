import { Type } from '@doesrobbiedream/ts-utils'
import { SourceEvent } from '../../event-artisan'
import { Projection } from '../projection.class'
import {
  EventProjectorMethod,
  EventType,
  MethodKey,
  ProjectorEventHandlerManifest,
} from './helpers/decorators.types'
import { eventApplierHandlerBuilder } from './helpers/event-applier-handler.builder'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export function EventApplierDecorator(...events: Array<Type<SourceEvent>>) {
  return (
    target: Projection<unknown>,
    methodName: MethodKey,
    descriptor: TypedPropertyDescriptor<EventProjectorMethod<SourceEvent>>
  ) => {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionEventHandlers,
        target
      ) || new Map()

    events.forEach((event) => {
      handlers.set(SourceEvent.getSignature(event), {
        handlerKey: methodName,
        Event: event,
      })
    })

    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionEventHandlers,
      handlers,
      target
    )
    const originalHandler = descriptor.value
    const newHandler = eventApplierHandlerBuilder(originalHandler)

    Object.defineProperty(newHandler, 'name', { value: methodName })

    return {
      value: newHandler,
    }
  }
}
