import { Type } from '@doesrobbiedream/ts-utils'
import { SourceEvent } from '../../event-artisan'
import { Projector } from '../projector.class'
import { Projection } from '../projection.class'
import {
  EventType,
  MethodKey,
  ProjectorHandlerManifest,
  UnpackArray,
  UnpackType,
} from './helpers/decorators.types'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export function BeforeAnyDecorator<T extends Type<SourceEvent>[]>() {
  return (
    target: Projector<Projection<unknown>>,
    methodName: MethodKey,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    descriptor: TypedPropertyDescriptor<
      (event: UnpackType<UnpackArray<T>>) => Promise<void>
    >
  ) => {
    const handlers: Map<EventType, ProjectorHandlerManifest> =
      Reflect.getMetadata(
        ProjectionDecoratedKeys.ProjectionBeforeAnyEventCallback,
        target
      ) || new Map()
    handlers.set(methodName, {
      handlerKey: methodName,
    })
    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionBeforeAnyEventCallback,
      handlers,
      target
    )
  }
}
