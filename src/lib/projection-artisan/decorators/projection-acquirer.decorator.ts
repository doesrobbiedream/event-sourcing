// Decorators
import { Projection } from '../projection.class'
import { Type } from '@doesrobbiedream/ts-utils'
import { Event, SourceEvent } from '../../event-artisan'
import { Projector } from '../projector.class'
import { EventDecoratedKeys } from '../../event-artisan/event.decorators'
import {
  EventType,
  MethodKey,
  ProjectorEventHandlerManifest,
} from './helpers/decorators.types'
import { ProjectionDecoratedKeys } from './helpers/projection.decorated-keys'

export type ProjectionAcquirerMethod<P extends Projection<unknown>> = (
  event: Event
) => Promise<P | P[]>

export function ProjectionAcquirerDecorator<P extends Projection<unknown>>(
  ...events: Array<Type<SourceEvent>>
) {
  return (
    target: Projector<P>,
    propertyKey: MethodKey,
    /* eslint-disable @typescript-eslint/no-unused-vars */
    _: TypedPropertyDescriptor<ProjectionAcquirerMethod<P>>
  ) => {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(ProjectionDecoratedKeys.ProjectionAcquirer, target) ||
      new Map()

    events.forEach((event) => {
      const type = Reflect.getMetadata(EventDecoratedKeys.Type, event)
      const version = Reflect.getMetadata(EventDecoratedKeys.Version, event)
      handlers.set(`${type}@${version}`, {
        handlerKey: propertyKey,
        Event: event,
      })
    })

    Reflect.defineMetadata(
      ProjectionDecoratedKeys.ProjectionAcquirer,
      handlers,
      target
    )
  }
}
