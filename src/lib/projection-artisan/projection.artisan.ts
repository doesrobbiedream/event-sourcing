import { ProjectionAcquirerDecorator } from './decorators/projection-acquirer.decorator'
import { EventApplierDecorator } from './decorators/event-applier.decorator'
import { ProjectorDecorator } from './decorators/projector.decorator'
import { AfterEachOfTypeDecorator } from './decorators/after-each-of-type.decorator'
import { AfterAny } from './decorators/after-any.decorator'
import { BeforeEachOfTypeDecorator } from './decorators/before-each-of-type.decorator'
import { BeforeAnyDecorator } from './decorators/before-any.decorator'

export class ProjectionArtisan {
  public static Projector = ProjectorDecorator
  public static ProjectionAcquirer = ProjectionAcquirerDecorator
  public static EventApplier = EventApplierDecorator
  public static BeforeAny = BeforeAnyDecorator
  public static BeforeEachOfType = BeforeEachOfTypeDecorator
  public static AfterEachOfType = AfterEachOfTypeDecorator
  public static AfterAny = AfterAny
}
