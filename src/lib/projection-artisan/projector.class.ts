import { Projection } from './projection.class'
import {
  filter,
  firstValueFrom,
  map,
  mergeWith,
  Observable,
  Subject,
} from 'rxjs'
import {
  HooksMap,
  ProjectorEventHandlerManifest,
  ProjectorHooks,
} from './decorators/helpers/decorators.types'
import { Event, SourceEvent } from '../event-artisan'
import { Queue } from 'queue-typescript'
import { ProjectionDecoratedKeys } from './decorators/helpers/projection.decorated-keys'
import { instanceToPlain } from 'class-transformer'
import { AnyObject } from '@doesrobbiedream/ts-utils'

interface EventApplicationManifest<P extends Projection<unknown>> {
  event: Event
  initial: Record<string, any>[]
  projected: P[]
}

class CompletionSignalEmitter extends Subject<void> {
  public static isCompletionSignalEmitter(
    emitter: unknown
  ): emitter is CompletionSignalEmitter {
    return emitter instanceof CompletionSignalEmitter
  }
}

export abstract class Projector<P extends Projection<any>> {
  public _clearedQueue$: Subject<void> = new Subject<void>()

  protected isSupportedEvent(event: Event): boolean {
    return this.projectionAcquirerManifests.has(
      `${event.type}@${event.version}`
    )
  }

  protected _start$: Subject<void> = new Subject()
  protected _appliedEvent$: Subject<EventApplicationManifest<P>> = new Subject()
  protected _stream$: Subject<Event> = new Subject()
  protected _tick$: Observable<void> = this._start$.pipe(
    mergeWith(this._appliedEvent$),
    map(() => null)
  )

  protected activeEventsBatch: Queue<Event | CompletionSignalEmitter>
  protected loadedEventBatches: Queue<Queue<Event | CompletionSignalEmitter>> =
    new Queue<Queue<Event | CompletionSignalEmitter>>()
  private readonly hooks: Map<ProjectorHooks, HooksMap>
  private readonly projectionAcquirerManifests: Map<
    string,
    ProjectorEventHandlerManifest
  >

  constructor() {
    // Reflected Data
    this.projectionAcquirerManifests =
      Reflect.getMetadata(ProjectionDecoratedKeys.ProjectionAcquirer, this) ||
      new Map()

    this.hooks = new Map()
    const [beforeAnyKey, beforeEachOfTypeKey, afterEachOfTypeKey, afterAnyKey] =
      [
        ProjectionDecoratedKeys.ProjectionBeforeAnyEventCallback,
        ProjectionDecoratedKeys.ProjectionBeforeEachOfTypeEventCallback,
        ProjectionDecoratedKeys.ProjectionAfterEachOfTypeEventCallback,
        ProjectionDecoratedKeys.ProjectionAfterAnyEventCallback,
      ]

    const beforeAnyMap = Reflect.getMetadata(beforeAnyKey, this)
    const beforeEachOfTypeMap = Reflect.getMetadata(beforeEachOfTypeKey, this)
    const afterEachOfTypeMap = Reflect.getMetadata(afterEachOfTypeKey, this)
    const afterAnyMap = Reflect.getMetadata(afterAnyKey, this)

    this.hooks.set(ProjectorHooks.beforeAny, beforeAnyMap || new Map())
    this.hooks.set(ProjectorHooks.beforeEach, beforeEachOfTypeMap || new Map())
    this.hooks.set(ProjectorHooks.afterEach, afterEachOfTypeMap || new Map())
    this.hooks.set(ProjectorHooks.afterAny, afterAnyMap || new Map())

    this.connectEventsStream()
    this._tick$.subscribe(() => this.tryNext())
  }

  public get clearedQueue$() {
    return this._clearedQueue$.asObservable()
  }

  public attachEventStream(stream: Observable<Event>) {
    const afterAppliedStreamedEvent = new Subject<Event>()
    stream
      .pipe(filter((event) => this.isSupportedEvent(event)))
      .subscribe((event) => {
        const finisher = new CompletionSignalEmitter()
        this.loadedEventBatches.enqueue(
          new Queue<Event | CompletionSignalEmitter>(event, finisher)
        )
        finisher.subscribe({
          next: () => {
            afterAppliedStreamedEvent.next(event)
            this.tryNext()
          },
          error: (error) => afterAppliedStreamedEvent.error({ error, event }),
        })
        if (!this.activeEventsBatch) this._start$.next()
      })
    return {
      afterAppliedStreamedEvent,
    }
  }

  public projectEvents(events: Array<Event>) {
    const supportedEvents = events.filter((event) =>
      this.isSupportedEvent(event)
    )
    const completionSignalEmitter = new CompletionSignalEmitter()
    const eventsBatch = new Queue<Event | CompletionSignalEmitter>()
    supportedEvents.forEach((e) => eventsBatch.enqueue(e))
    eventsBatch.enqueue(completionSignalEmitter)
    this.loadedEventBatches.enqueue(eventsBatch)
    if (this.loadedEventBatches.length === 1) this._start$.next()
    return {
      batchFinished: firstValueFrom(completionSignalEmitter),
    }
  }

  private connectEventsStream() {
    this._stream$.subscribe(async (event) => {
      await this.applyHook(this.hooks.get(ProjectorHooks.beforeAny), event)
      await this.applyHook(
        this.hooks.get(ProjectorHooks.beforeEach),
        event,
        event
      )
      const { projections, values } = await this.acquireCurrentState(event)
      await this.applyEvent(event, projections)
      await this.applyHook(
        this.hooks.get(ProjectorHooks.afterEach),
        projections,
        event
      )
      await this.applyHook(this.hooks.get(ProjectorHooks.afterAny), projections)

      this._appliedEvent$.next({
        initial: values,
        projected: projections,
        event,
      })
    })
  }

  private tryNext() {
    if (!this.activeEventsBatch || !this.activeEventsBatch.length) {
      this.activeEventsBatch = this.loadedEventBatches.dequeue()
    }

    if (this.activeEventsBatch) {
      const nextQueuedElement = this.activeEventsBatch.dequeue()
      if (
        CompletionSignalEmitter.isCompletionSignalEmitter(nextQueuedElement)
      ) {
        nextQueuedElement.next()
      } else {
        this._stream$.next(nextQueuedElement)
      }
    } else {
      this._clearedQueue$.next()
    }
  }

  private async acquireCurrentState(
    event: Event
  ): Promise<{ projections: Array<P>; values: Array<AnyObject> }> {
    const eventIndex = SourceEvent.getSignatureFromRaw(event)
    if (!this.projectionAcquirerManifests.has(eventIndex)) {
      throw Error(
        `Event ${event.type}@${event.version} has no fetchers to acquire current projections. Please, define a handler using @ProjectionAcquirer decorator`
      )
    }
    const { handlerKey: eventApplierKey } =
      this.projectionAcquirerManifests.get(eventIndex)
    return await this[eventApplierKey](event).then((results) => ({
      projections: Array.isArray(results) ? results : [results],
      values: instanceToPlain(results) as Array<AnyObject>,
    }))
  }

  private async applyEvent(
    event: Event,
    initialProjectionState: Array<P>
  ): Promise<Array<P>> {
    const projections: Array<P> = [...initialProjectionState]
    const projectionsEventsApplierMap: (data: P) => Promise<P> = (projection) =>
      projection.apply(event).then(() => projection)
    return await Promise.all(projections.map(projectionsEventsApplierMap))
  }

  private async applyHook(
    hooksMap: HooksMap,
    data: unknown,
    onlyOnEvent?: Event
  ) {
    if (onlyOnEvent) {
      const eventSignature = SourceEvent.getSignatureFromRaw(onlyOnEvent)
      if (hooksMap.has(eventSignature)) {
        const { handlerKey } = hooksMap.get(eventSignature)
        await this[handlerKey](data)
      }
    } else {
      for (const [_, hook] of hooksMap) {
        const { handlerKey } = hook
        await this[handlerKey](data)
      }
    }
  }
}
