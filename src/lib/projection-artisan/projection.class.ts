import { Event } from '../event-artisan/event.types'
import {
  EventType,
  ProjectorEventHandlerManifest,
} from './decorators/helpers/decorators.types'
import { Type } from '@doesrobbiedream/ts-utils'
import { instanceToPlain, plainToInstance } from 'class-transformer'
import { ProjectionDecoratedKeys } from './decorators/helpers/projection.decorated-keys'

type extractProjectionDataType<T> = T extends Projection<infer D> ? D : never

export class Projection<T> {
  public static fromInitialState<P extends Projection<unknown>>(
    t: Type<P>,
    data?: Partial<extractProjectionDataType<P>>
  ): P {
    return plainToInstance(t, data || {})
  }

  async apply(event: Event): Promise<void> {
    const handlers: Map<EventType, ProjectorEventHandlerManifest> =
      Reflect.getMetadata(ProjectionDecoratedKeys.ProjectionEventHandlers, this)
    const selector = `${event.type}@${event.version}`
    const handlerKey = handlers.get(selector).handlerKey

    await this[handlerKey](event)
  }

  public serialize(): T {
    return instanceToPlain(this) as T
  }
}
