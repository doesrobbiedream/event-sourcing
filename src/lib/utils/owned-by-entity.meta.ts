export interface OwnedByEntityEventMeta {
  owned_by: string
}
