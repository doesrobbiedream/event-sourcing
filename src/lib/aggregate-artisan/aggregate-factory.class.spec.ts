import { Aggregate } from './aggregate-root.class'
import { MixinArtisan, MixinsComposer } from '@doesrobbiedream/ts-utils'
import { IsNotEmpty, ValidateNested } from 'class-validator'
import { Type } from 'class-transformer'
import { AggregateFactory } from './aggregate-factory.class'

class DeepPropertyObject {
  @IsNotEmpty()
  withProperty: string
}

class ValidatableClass {
  @IsNotEmpty()
  someString: string

  @Type(() => DeepPropertyObject)
  @ValidateNested()
  deepPropertyObject: DeepPropertyObject
}

class ValidatableClassAggregate extends MixinsComposer(
  [MixinArtisan.craft(ValidatableClass)],
  Aggregate
) {}

describe(AggregateFactory.name, () => {
  it('should load the provided data into the aggregate instance', async () => {
    const featureRequestData: ValidatableClass = {
      someString: 'I am some string',
      deepPropertyObject: {
        withProperty: 'Should not be empty',
      },
    }
    const aggregate = await AggregateFactory.initializeFromState(
      ValidatableClassAggregate,
      featureRequestData,
      { validateAs: ValidatableClass }
    )
    expect(aggregate).toBeInstanceOf(ValidatableClassAggregate)
  })
  it('should prevent from loading erroneous data into aggregates', async () => {
    const featureRequestData: ValidatableClass = {
      someString: 'I am some string',
      deepPropertyObject: {
        withProperty: '',
      },
    }
    await expect(() =>
      AggregateFactory.initializeFromState(
        ValidatableClassAggregate,
        featureRequestData,
        { validateAs: ValidatableClass }
      )
    ).rejects.toThrow()
  })
})
