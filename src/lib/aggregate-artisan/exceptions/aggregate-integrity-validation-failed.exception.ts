import { CustomException } from '@doesrobbiedream/nest-catcher'
import { ValidationError } from 'class-validator'

export class AggregateIntegrityValidationFailed extends CustomException {
  protected exception = 'AGGREGATE_INTEGRITY_VALIDATION_FAILED'

  constructor(protected errors: ValidationError[]) {
    super()
  }
}
