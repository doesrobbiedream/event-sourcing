import { CustomException } from '@doesrobbiedream/nest-catcher'

export class AggregateHandlerNotRegisteredException extends CustomException {
  exception = 'AGGREGATE_HANDLER_NOT_DEFINED'
  message: string

  constructor(event_type: string, event_version: number) {
    super()
    this.message = `No handler was registered to process ${event_type}@${event_version}`
  }
}
