import { Event } from '../event-artisan'
import { AggregateDecoratedKeys } from './aggregate.decorators'
import { AggregateHandlerNotRegisteredException } from './exceptions/aggregate-handler-not-registered.exception'

export class Aggregate {
  public async updateStateFromEvents(event: Event | Event[]): Promise<void> {
    const events: Event[] = Array.isArray(event) ? event : [event]
    for (const event of events) {
      await this.executeEvent(event)
    }
  }

  public async applyEvent(event: Event): Promise<void> {
    return this.executeEvent(event)
  }

  private async executeEvent(event: Event) {
    const handlersDictionary = Reflect.getMetadata(
      AggregateDecoratedKeys.DeclaredEventHandlers,
      this.constructor
    )
    const handler = handlersDictionary[`${event.type}@${event.version}`]
    if (handler === undefined || typeof this[handler] !== 'function') {
      throw new AggregateHandlerNotRegisteredException(
        event.type,
        event.version
      )
    }
    await this[handler](event)
  }
}
