import { Type } from '@doesrobbiedream/ts-utils'
import { plainToInstance } from 'class-transformer'
import { validate } from 'class-validator'

export class AggregateFactory {
  public static async initializeFromState<T extends object>(
    as: Type<T>,
    withData: unknown,
    options?: { validateAs?: Type<object> }
  ): Promise<T> {
    const validatable = plainToInstance(options?.validateAs || as, withData)
    const validation = await validate(validatable)
    if (validation.length) {
      throw Error()
    }
    return plainToInstance(as, withData)
  }
}
