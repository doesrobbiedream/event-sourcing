import {
  AggregateDecorator,
  EventHandlerDecorator,
} from './aggregate.decorators'

export class AggregateArtisan {
  public static EventHandler = EventHandlerDecorator
  public static Aggregate = AggregateDecorator
}
