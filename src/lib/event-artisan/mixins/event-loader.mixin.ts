import { Event } from '../event.types'
import { MixinArtisan } from '@doesrobbiedream/ts-utils'

export class EventLoader implements Event {
  type: string
  version: number

  uuid: string
  data: unknown
  meta: unknown

  createdAt: Date

  fromStoredData<Data = unknown, Meta = unknown>(event: Event<Data, Meta>) {
    this.uuid = event.uuid
    this.data = event.data
    this.meta = event.meta

    this.createdAt = event.createdAt
  }

  fromRawData<Data = unknown, Meta = unknown>(data: Data, meta: Meta) {
    this.data = data
    this.meta = meta
  }
}

export const EventLoaderMixin = MixinArtisan.craft(EventLoader)
