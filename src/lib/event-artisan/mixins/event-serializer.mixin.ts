import { MixinArtisan } from '@doesrobbiedream/ts-utils'
import { Event } from '../event.types'

export class EventSerializer {
  serialize(): Event {
    return [...Object.getOwnPropertyNames(this), 'version', 'type'].reduce(
      (serialization, key: string) => {
        const propertyKey = key as keyof this
        return { ...serialization, [propertyKey]: this[propertyKey] }
      },
      {}
    ) as Event
  }
}

export const EventSerializerMixin = MixinArtisan.craft(EventSerializer)
