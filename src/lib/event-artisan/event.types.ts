import { SourceEvent } from './source-event.class'

export type EmptyObject = { [key: string]: never }

export interface Upcaster<L extends SourceEvent = SourceEvent> {
  upcast(event: L): void
}

export interface Event<Data = unknown, Meta = unknown> {
  uuid: string
  type: string
  version: number
  createdAt: Date
  data: Data
  meta: Meta
}

export type ExtractDataFromEvent<T extends Event> = T extends Event<
  infer D,
  infer _
>
  ? D
  : never
export type ExtractMetaFromEvent<T extends Event> = T extends Event<
  infer _,
  infer M
>
  ? M
  : never

export type ExtractDataFromSourceEvent<T extends SourceEvent> =
  T extends SourceEvent<infer D, infer _> ? D : never
export type ExtractMetaFromSourceEvent<T extends SourceEvent> =
  T extends SourceEvent<infer _, infer M> ? M : never

export interface EventCraftingManifest {
  type: string
  version: number
}
