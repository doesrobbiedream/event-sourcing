import { CustomException } from '@doesrobbiedream/nest-catcher'

export class NotSupportedEventException extends CustomException {
  exception: 'NOT_SUPPORTED_EVENT'
  message: string

  constructor(event_signature: string) {
    super()
    this.message = `Event ${event_signature} is not supported by this EventFactory`
  }
}
