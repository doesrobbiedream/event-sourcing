import {
  EventDecorator,
  LegacyEventDecorator,
  TypeDecorator,
  VersionDecorator,
} from './event.decorators'
import { EventFactory } from './event-factory.class'

export class EventArtisan {
  public static EventFactory = EventFactory
  public static Event = EventDecorator
  public static LegacyOf = LegacyEventDecorator
  public static Version = VersionDecorator
  public static Type = TypeDecorator
}
