import { SourceEvent } from './source-event.class'
import { Type } from '@doesrobbiedream/ts-utils'
import {
  Event,
  ExtractDataFromSourceEvent,
  ExtractMetaFromSourceEvent,
} from './event.types'
import { plainToInstance } from 'class-transformer'
import { validateSync } from 'class-validator'
import { EventDecoratedKeys } from './event.decorators'
import { NotSupportedEventException } from './exceptions/not-supported-event.exception'

export class EventFactory {
  public static eventSignature(event: Event) {
    return `${event.type}@${event.version}`
  }

  public static supportEvent(
    type: string,
    version: number,
    constructor: Type<SourceEvent>
  ) {
    const supported = this.supportedEvents
    supported.set(`${type}@${version}`, constructor)
    this.supportedEvents = supported
  }

  protected static get supportedEvents(): Map<string, Type<SourceEvent>> {
    return (
      Reflect.getMetadata(EventDecoratedKeys.SupportedEvents, EventFactory) ||
      new Map()
    )
  }

  protected static set supportedEvents(
    supported: Map<string, Type<SourceEvent>>
  ) {
    Reflect.defineMetadata(
      EventDecoratedKeys.SupportedEvents,
      supported,
      EventFactory
    )
  }

  public static craftFromData<T extends SourceEvent>(
    event: Type<T>,
    data: ExtractDataFromSourceEvent<T>,
    meta: ExtractMetaFromSourceEvent<T>
  ): T {
    const constructed = plainToInstance(event, { data, meta })
    validateSync(constructed)
    return constructed
  }

  public static craftFromEvent(event: Event) {
    const eventSignature = this.eventSignature(event)
    const constructor = this.supportedEvents.get(eventSignature)
    if (!constructor) throw new NotSupportedEventException(eventSignature)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { version, type, ...mutable } = event
    const constructed = plainToInstance(constructor, mutable)
    validateSync(constructed)
    return constructed
  }
}
