import { EventFactory } from './event-factory.class'
import { SourceEvent } from './source-event.class'
import { EventArtisan } from './event.artisan'

@EventArtisan.Event({
  type: MyTestSourceEvent.name,
  version: 1,
})
class MyTestSourceEvent extends SourceEvent<{ foo: string }, { baz: string }> {}

describe(EventFactory.name, function () {
  const event = EventFactory.craftFromData(
    MyTestSourceEvent,
    { foo: 'Foo' },
    { baz: 'Baz' }
  )
  describe('when crafting events from data', function () {
    it('should return a populated source event', () => {
      expect(event).toBeInstanceOf(MyTestSourceEvent)
      expect(event.serialize()).toEqual(
        expect.objectContaining({ data: { foo: 'Foo' }, meta: { baz: 'Baz' } })
      )
    })
  })
  describe('when crafting events from stored events', function () {
    const storedEvent = event.serialize()
    it('should return the corresponding SourceEvent', () => {
      const craftedFromStoredEvent = EventFactory.craftFromEvent(storedEvent)
      expect(craftedFromStoredEvent).toBeInstanceOf(MyTestSourceEvent)
    })
  })
})
