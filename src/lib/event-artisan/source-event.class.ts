import { Event } from './event.types'
import { EventArtisan } from './event.artisan'
import { MixinsComposer, NoopMixinBase, Type } from '@doesrobbiedream/ts-utils'
import { EventLoaderMixin } from './mixins/event-loader.mixin'
import { EventSerializerMixin } from './mixins/event-serializer.mixin'
import { LegacyEventMixin } from './mixins/legacy-event.mixin'
import { EventDecoratedKeys } from './event.decorators'
import { v4 } from 'uuid'

export class SourceEvent<Data = unknown, Meta = unknown>
  extends MixinsComposer(
    [LegacyEventMixin, EventLoaderMixin, EventSerializerMixin],
    NoopMixinBase
  )
  implements Event<Data, Meta>
{
  @EventArtisan.Type
  public type: string
  @EventArtisan.Version
  public version: number
  public uuid: string
  public createdAt: Date
  public data: Data
  public meta: Meta

  constructor() {
    super()
    this.uuid = v4()
  }

  public static getSignature(event: Type<SourceEvent>) {
    const type = Reflect.getMetadata(EventDecoratedKeys.Type, event)
    const version = Reflect.getMetadata(EventDecoratedKeys.Version, event)
    return `${type}@${version}`
  }

  public static getSignatureFromRaw(event: Event) {
    return `${event.type}@${event.version}`
  }
}
